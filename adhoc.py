# encoding: utf-8

from tableutil import Table, Cell, Row, HEADING, CLASSES, JUSTIFY, Justify
from dominate import tags


def basic_examples():
    title_cell = Cell(value=u'multi\nline\ntitle',
                      href=u'http://www.apple.com')

    print(title_cell.text(width=title_cell.width))
    print(u'-----')
    print(title_cell.jira())
    print(u'-----')
    print(title_cell.html())

    t = Table(title=title_cell,
              headings=[{HEADING: u'x'}, {HEADING: u'yyy'}, {HEADING: u'xz'}],
              show_column_headings=True,
              row_numbers=True,
              show_summaries=True)

    t.add_heading_row([{HEADING: u''}, {HEADING: u'Spanning', u'colspan': 2}])

    sub_table = Cell(Table.init_from_tree([Cell(value=u'd-a (google)',
                                                href=u'http://google.co.uk'),
                                           Cell(value=u'd-b',
                                                classes=['text-success'])]))

    t2 = Table(title=Cell(u''),
               headings=[{HEADING: u'A'}, {u'heading': u'B'}],
               show_column_headings=False,
               row_numbers=True,
               show_summaries=True)

    t2.add_rows([
        {u'A': u'1', u'B': u'3'},
        {u'A': u'2', u'B': u'2'},
        {u'A': u'3', u'B': u'1'}
    ])
    row_ef = Row(headings=t.column_headings,
                 row={u'x': u'e', u'yyy': u'f'},
                 classes=['table-info'])

    t.add_rows([
        {u'x': u'a', u'yyy': Cell(tags.i(_class='fa fa-spinner fa-spin fa-3x fa-fw'))},
        row_ef,
        {u'x': u'c', u'yyy': sub_table},
        {u'x': Cell(u'g', classes=['text-success', 'table-secondary']), u'yyy': u'h', u'_classes': ['table-warning', 'text-danger']}
    ])
    t.add_row(row={u'x': u'i', u'yyy': t2},
              classes=['table-success'])

    t.add_row(row={u'x': u'k',
                   u'yyy': tags.img(src='http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/sign-check-icon.png',
                                    height='20',
                                    title='test_title',
                                    style='background-color:#566573')})

    t.add_summary(u'yyy', u'Some summary data...')

    t.write_to_html(filename=u'adhoc',
                    html_folder=u'/Users/davisowb/Desktop/',
                    open_in_browser=True,
                    sort_column=u'x')

    #empty_t = Table(title=Cell(u''),
    #                headings=[{u'heading': u'x'}, {u'heading': u'yyy'}, {u'heading': u'xz'}],
    #                show_column_headings=True,
    #                row_numbers=True,
    #                show_summaries=True)

    #empty_t.write_to_html(filename=u'adhoc_empty',
    #                      html_folder=u'/Users/davisowb/Desktop/',
    #                      open_in_browser=True,
    #                      sort_column=u'x')


def grouped_heading_examples():
    title_cell = Cell(value=u'Grouped Heading Example',
                      href=u'http://www.google.com')

    t3 = Table(title=title_cell,
               headings=[
                   {
                       HEADING: u'topgroupA.group1.heading1'
                   },
                   {
                       HEADING: u'topgroupA.group1.heading2',
                       CLASSES: [u'text-danger']
                   },
                   {
                       HEADING: u'topgroupB.group2.heading4',
                       CLASSES: {
                           u'topgroupB': [u'bg-danger'],
                       }
                   },
                   {
                       HEADING: u'topgroupB.group2.heading3',
                       CLASSES: [u'text-info']
                   },
                   {
                       HEADING: u'topgroupA.group1.heading5',
                       JUSTIFY: Justify.right
                   },
                   {
                       HEADING: u'topgroupB.group3.heading6',
                       CLASSES: {
                           u'heading6': [u'text-info'],
                           u'group3': [u'text-primary'],
                           u'topgroupB': [u'text-success'],
                       }
                   },
                   {
                       HEADING: u'heading7'
                   }
               ],
               headings_delimiter=u'.',
               show_summaries=True)

    t3.add_row(row={
                   u'topgroupA.group1.heading1': u'B',
                   u'topgroupA.group1.heading2': u'C',
                   u'topgroupB.group2.heading3': u'F',
                   u'topgroupB.group2.heading4': u'E',
                   u'topgroupA.group1.heading5': u'D',
                   u'topgroupB.group3.heading6': u'G',
                   u'heading7': u'A'
               },
               classes=['table-info'])

    t3.add_row(row={
                   u'topgroupB.group2.heading4': 123,
                   u'heading7': u'XYZ'
               },
               classes=['table-warning'])

    t3.write_to_html(filename=u'adhoc2',
                     html_folder=u'/Users/davisowb/Desktop/',
                     open_in_browser=True)


basic_examples()
grouped_heading_examples()
